require 'hyperresource'
require 'highline'
require 'pry'

require './cli/applications/create'
require './cli/applications/index'
require './cli/applications/show'

require './cli/deploys/index'
require './cli/deploys/show'

require './cli/errors/index'
require './cli/errors/show'

require './cli/users/create'
require './cli/users/login'
require './cli/users/logout'
require './cli/users/show'

require './cli/root_form'
require './api/backend_api'
require './router'

module Cli
  CLI = HighLine.new
  HighLine.color_scheme = HighLine::SampleColorScheme.new

  API = BackendApi.new
  STATE = {
    application_id: nil,
    deploy_id: nil,
    error_id: nil
  }

  def self.call
    RootForm.call
  end
end

Cli.call
