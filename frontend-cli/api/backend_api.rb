class BackendApi < HyperResource
  self.root = 'http://backendapi:4567'
  self.headers = {
    'Accept' => 'application/json',
    'Content-Type' => 'application/json'
  }
end
