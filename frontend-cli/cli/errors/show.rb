module Cli
  module Errors
    class Show
      def self.route_after
        Errors::Index
      end

      def self.call(error_id)
        error = API.error(id: error_id)
        CLI.say(HighLine.color("Error message: #{error.message}", :info))
        CLI.say(HighLine.color('Backtrace:', :info))

        error.backtrace.each do |line|
          CLI.say(HighLine.color(line, :warning))
        end
      end
    end
  end
end
