module Cli
  module Errors
    class Index
      def self.call
        source = if STATE[:deploy_id]
                   API.deploy(id: STATE[:deploy_id]).get
                 else
                   API.application(id: STATE[:application_id]).get
                 end

        errors = source.errors.errors

        CLI.say(HighLine.color('Showing errors', :info))
        CLI.choose do |menu|
          menu.prompt = 'Which error would you like to see'

          errors.each do |error|
            menu.choice(
              error.id,
              '',
              error.message,
              &Router.route(Show)
            )
          end

          menu.choice(
            STATE[:application_id],
            '',
            'Go back to application',
            &Router.route(Applications::Show)
          )

          if STATE[:deploy_id]
            menu.choice(
              STATE[:deploy_id],
              '',
              'Go back to deploy',
              &Router.route(Deploys::Show)
            )
          end
        end
      end
    end
  end
end
