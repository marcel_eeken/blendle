module Cli
  module Deploys
    class Index
      def self.call
        application = API.users.application(id: STATE[:application_id]).get
        deploys = application.deploys.deploys

        CLI.say(HighLine.color('Showing deploys', :info))
        CLI.choose do |menu|
          menu.prompt = 'Which deploy would you like to see'

          deploys.each do |deploy|
            menu.choice(
              deploy.id,
              '',
              "Deployed at: #{deploy.deploy_date}",
              &Router.route(Show)
            )
          end

          menu.choice(
            application.id,
            '',
            'Go back to application',
            &Router.route(Applications::Show)
          )
        end
      end
    end
  end
end
