module Cli
  module Deploys
    class Show
      def self.route_after
        Index
      end

      def self.call(deploy_id)
        STATE[:deploy_id] = deploy_id

        deploy = API.deploy(id: deploy_id)

        CLI.say(HighLine.color("Deploy date: #{deploy.deploy_date}", :info))
        CLI.say(HighLine.color("Deploy sha: #{deploy.git_sha}", :info))

        CLI.choose do |menu|
          menu.prompt = 'Next action'
          menu.choice(
            :errors,
            '',
            'Show errors',
            &Router.route(Errors::Index)
          )

          menu.choice(
            :index,
            '',
            'Go back to deploys',
            &Router.route(Index)
          )
        end
      end
    end
  end
end
