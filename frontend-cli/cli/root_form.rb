module Cli
  class RootForm
    def self.call
      CLI.choose do |menu|
        menu.prompt = 'Which action would you like to do'
        menu.choice(
          :create_account,
          '',
          'Create a new account',
          &Router.route(Users::Create)
        )

        menu.choice(
          :login_account,
          '',
          'Login to an existing account',
          &Router.route(Users::Login)
        )

        menu.choice(
          :exit,
          '',
          'Exit the application'
        )
      end
    end
  end
end
