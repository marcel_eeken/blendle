module Cli
  module Applications
    class Create
      def self.route_after
        Show
      end

      def self.call
        CLI.say('Please enter application information')
        name = CLI.ask('Name: ', String)

        application = API.users.applications.post(
          application: {
            name: name
          }
        )

        STATE[:application_id] = application.id
      end
    end
  end
end
