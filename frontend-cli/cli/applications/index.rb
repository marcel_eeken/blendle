module Cli
  module Applications
    class Index
      def self.call
        STATE[:application_id] = nil
        STATE[:deploy_id] = nil

        applications = API.users.applications.applications
        CLI.say(HighLine.color('Showing applications', :info))
        CLI.choose do |menu|
          menu.prompt = 'Which application would you like to see'

          applications.each do |application|
            menu.choice(
              application.id,
              '',
              application.name,
              &Router.route(Show)
            )

            menu.choice(
              :user,
              '',
              'Go back to my user',
              &Router.route(Users::Show)
            )
          end
        end
      end
    end
  end
end
