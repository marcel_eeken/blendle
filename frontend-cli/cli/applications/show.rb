module Cli
  module Applications
    class Show
      def self.call(chosen = nil)
        STATE[:deploy_id] = nil
        STATE[:application_id] = chosen if chosen

        application = API.users.application(id: STATE[:application_id]).get

        CLI.say(HighLine.color("Application: #{application.name}", :info))
        CLI.say(HighLine.color("API key: #{application.api_key}", :notice))

        CLI.choose do |menu|
          menu.prompt = 'Next action'
          menu.choice(
            :errors,
            '',
            'Show errors',
            &Router.route(Errors::Index)
          )

          menu.choice(
            :index,
            '',
            'Show deploys',
            &Router.route(Deploys::Index)
          )

          menu.choice(
            :index,
            '',
            'Go back to applications',
            &Router.route(Index)
          )
        end
      end
    end
  end
end
