module Cli
  module Users
    class Show
      def self.call
        user = API.users.get
        CLI.say(HighLine.color("Current user is #{user.name}", :info))

        CLI.choose do |menu|
          menu.prompt = 'What would you like to do'
          menu.choice(
            :applications,
            '',
            'Show my applications',
            &Router.route(Applications::Index)
          )

          menu.choice(
            :create_application,
            '',
            'Create an application',
            &Router.route(Applications::Create)
          )

          menu.choice(
            :logout,
            '',
            'Logout',
            &Router.route(Logout)
          )
        end
      end
    end
  end
end
