module Cli
  module Users
    class Logout
      def self.route_after
        RootForm
      end

      def self.call
        CLI.say(HighLine.color('Succesfully logged out', :info))

        API.auth = nil
      end
    end
  end
end
