module Cli
  module Users
    class Create
      def self.route_after
        Show
      end

      def self.call
        CLI.say('Please enter your account information')
        name = CLI.ask('Name: ', String)
        email = CLI.ask('Email: ', String)
        password = CLI.ask('Password: ', String) { |q| q.echo = '' }

        API.users.post(
          user: {
            name: name,
            email: email,
            password: password
          }
        )

        API.auth = {
          basic: [email, password]
        }
      end
    end
  end
end
