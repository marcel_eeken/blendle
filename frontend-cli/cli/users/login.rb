module Cli
  module Users
    class Login
      def self.route_after
        Show
      end

      def self.call
        CLI.say('Please enter your login information')
        email = CLI.ask('Email: ', String)
        password = CLI.ask('Password: ', String) { |q| q.echo = '' }

        API.auth = {
          basic: [email, password]
        }
      end
    end
  end
end
