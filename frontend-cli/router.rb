module Router
  def self.route(route_to)
    proc do |chosen|
      begin
        arity = route_to.method(:call).arity
        if arity == 1 || arity == -1
          route_to.call(chosen)
        else
          route_to.call
        end

        route_to.route_after.call if route_to.respond_to? :route_after
      rescue HyperResource::ClientError => _e
        Cli::API.auth = {
          basic: []
        }
        Cli::CLI.say(HighLine.color('Login failed', :error))
        Cli::RootForm.call
      rescue HyperResource::ServerError => _e
        Cli::CLI.say(HighLine.color('Something went wrong', :error))
        Cli::RootForm.call
      end
    end
  end
end
