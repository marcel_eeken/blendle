class GenErrorsController < ApplicationController
  def generate_error
    raise StandardError, 'Testing error logging'
  end
end
