#!/bin/bash
set -x

# Wait for the backendapi to be up and running
sleep 10

bundle check || bundle install

bundle exec rake errors:notify_deploy

exec bundle exec rails s -b 0.0.0.0
