require 'securerandom'
namespace :errors do
  desc 'Trigger deploy notification'
  task :notify_deploy do
    # Dummy sha because we are not in a repo
    commit = SecureRandom.hex(32)

    uri = URI('http://backendapi:4567/deploys')

    req = Net::HTTP::Post.new(
      uri,
      'Content-Type' => 'application/json',
      'AUTHORIZATION' => ENV.fetch('API_KEY')
    )

    req.body = {
      deploy: {
        git_sha: commit
      }
    }.to_json

    Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
  end
end
