module Middleware
  class ErrorReporter
    def initialize(app)
      @app = app
    end

    def call(env)
      begin
        response = @app.call(env)
      rescue Exception => ex
        log_error(ex)
        raise ex
      end

      response
    end

    private

    def log_error(ex)
      uri = URI('http://backendapi:4567/errors')

      req = Net::HTTP::Post.new(
        uri,
        'Content-Type' => 'application/json',
        'AUTHORIZATION' => ENV.fetch('API_KEY')
      )

      req.body = {
        error: {
          message: ex.message,
          backtrace: ex.backtrace
        }
      }.to_json

      Net::HTTP.start(uri.hostname, uri.port) do |http|
        p 'Testing request'
        http.request(req)
      end
    end
  end
end
