# Blendle sample

## Purpose of the application

The application is a simple error tracking application that can be used to
track errors that have happend in rack applications

## Architecture

The application consists of 3 basic models:
* Applications
* Deploys
* Errors

Besides that there is also a `User` models for authentication.

Deploys and errors can only be created through an api call with an api key.

## Technologies used

Database access is done with [Sequel](http://sequel.jeremyevans.net/) through
`Sequel::Model`. I have looked in to [rom-rb](http://rom-rb.org/) but found that it
lacked in good documentation making implementing it difficult.

Webrequest are handled through [Sinatra](http://www.sinatrarb.com/) and responses
are presented with [Roar](https://github.com/apotonick/roar).

## Sample applications

### Frontendcli

The FrontendCli is a simple cli application that talks to the backend api. With this you can create users, applications and view the deploys and errors of the created applications.

### ErrorGenerator

The ErrorGenerator is a simple rails application that is used for testing purpose. It has middleware that catches errors in the rails application and sends them to the BackendApi. The route for generating an error is `/generate_error`


## Running the applications

All the applications can be ran through docker-compose.

First you have to run `docker-compose build` to build all the docker images. After that
you can run `docker-compose run frontendcli bash` with which you enter the cli container, where you can run `bundle` and then `bundle exec ruby ./cli.rb`.

In the background the BackendApi and ErrorGenerator have been started. With normal docker techniques you are able to get the ip of the container of the ErrorGenerator to be able to trigger an error. On each start of the docker containers a deploy is triggerd.

There is a standard user created with the following data:
* name: Blendle
* email: info@blendle.nl
* password: 1234

This user is also used in the ErrorGenerator application, so it is probably best to login using this user. But if you want you can create a different user.

## BackendApi running tests

To run tests for the BackendApi you first have to build the images using
`docker-compose build`. After this you can start an container for the BackendApi
with `docker-compose run backendapi bash` in which you can then run `bundle` and
`bundle exec guard` which will monitor your files and run the features.

## Possible errors

Error: `A server is already running. Check /home/app/tmp/pids/server.pid.
Exiting`
This means you already have a container for the ErrorGenerator running with the
Rails application available. Make sure no other containers for the ErrorGenerator
are running.
