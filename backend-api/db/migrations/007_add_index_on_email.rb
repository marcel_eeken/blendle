# frozen_string_literal: true

Sequel.migration do
  change do
    add_index :users, :email, unique: true
  end
end
