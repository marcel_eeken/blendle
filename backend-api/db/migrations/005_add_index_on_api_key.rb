# frozen_string_literal: true

Sequel.migration do
  change do
    add_index :applications, :api_key, unique: true
  end
end
