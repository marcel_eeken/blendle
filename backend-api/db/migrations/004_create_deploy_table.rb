# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:deploys) do
      primary_key :id
      DateTime :deploy_date, null: false
      String :git_sha, null: false
      Integer :application_id, null: false
    end
  end
end
