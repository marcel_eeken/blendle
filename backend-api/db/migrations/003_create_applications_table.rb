# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:applications) do
      primary_key :id
      String :name, null: false
      Integer :user_id, null: false
      String :api_key, null: false, unique: true
    end
  end
end
