# frozen_string_literal: true

Sequel.migration do
  change do
    add_index :applications, :user_id
    add_index :errors, :application_id
    add_index :errors, :deploy_id
    add_index :deploys, :application_id
  end
end
