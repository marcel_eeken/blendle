# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:errors) do
      primary_key :id
      String :message, null: false
      Integer :application_id, null: false
      Integer :deploy_id
      column :backtrace, 'text[]', default: []
    end
  end
end
