# frozen_string_literal: true

Sequel.migration do
  change do
    add_column :users, :encrypted_password, String, null: false
  end
end
