# frozen_string_literal: true

require 'sequel'

host = ENV.fetch('POSTGRES_HOST')
db = ENV.fetch('POSTGRES_DB')
user = ENV.fetch('POSTGRES_USER')
password = ENV.fetch('POSTGRES_PASSWORD')

Sequel.extension :pg_array_ops

DB = Sequel.connect(
  "postgres://#{host}/#{db}?user=#{user}&password=#{password}"
)
DB.extension :pg_array

if %w(development).include?(ENV.fetch('RACK_ENV', 'development'))
  require 'logger'
  DB.loggers << Logger.new($stdout)
end
