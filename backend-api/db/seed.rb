# frozen_string_literal: true

user = User.find_or_create(name: 'Blendle', email: 'info@blendle.nl') do |user|
  user.password = '1234'
end

application = Application.find_or_create(name: 'Blendle', user_id: user.id)
application.update(api_key: 'abdefghij')
