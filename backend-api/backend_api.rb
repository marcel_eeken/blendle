# frozen_string_literal: true

$LOAD_PATH << File.expand_path('../', __FILE__)

require 'rubygems'
require 'bundler'

Bundler.require

if %w(development test).include?(ENV.fetch('RACK_ENV', 'development'))
  require 'pry'
end

require 'warden'

require 'db/init'

require 'app/middleware/json_body_parser'
require 'app/controllers/controllers'
require 'app/forms/forms'
require 'app/presenters/presenters'
require 'app/helpers/warden_helper'
require 'app/helpers/api_authentication_helper'

module BackendApi
  class App < Sinatra::Application
    configure do
      disable :method_override
      disable :static
    end

    configure :production, :development do
      enable :logging
    end

    use Rack::Deflater
    use Rack::Session::Cookie, secret: ENV.fetch('RACK_SECRET')
    use Rack::JsonBodyParser

    use Warden::Manager do |manager|
      manager.default_strategies [:basic, :password]
      manager.intercept_401 = false
      manager.failure_app = BackendApi::App
      manager.serialize_into_session(&:id)
      manager.serialize_from_session { |id| User.find(id: id) }
    end

    Warden::Manager.before_failure do |env, _opts|
      env['REQUEST_METHOD'] = 'POST'
    end

    Warden::Strategies.add(:password) do
      def valid?
        params[:email] && params[:password]
      end

      def authenticate!
        user = User.authenticate(params[:email], params[:password])
        user.nil? ? fail!('Could not log in') : success!(user)
      end
    end

    Warden::Strategies.add(:basic) do
      def auth
        @auth ||= Rack::Auth::Basic::Request.new(env)
      end

      def valid?
        auth.provided? && auth.basic? && auth.credentials
      end

      def authenticate!
        user = User.authenticate(
          auth.credentials.first,
          auth.credentials.last
        )
        user.nil? ? fail!('Could not log in') : success!(user)
      end

      def store?
        false
      end
    end

    helpers Sinatra::WardenHelper
    helpers Sinatra::ApiAuthenticationHelper

    use BackendApi::Controllers::ErrorsController
    use BackendApi::Controllers::UsersController
    use BackendApi::Controllers::SessionsController
    use BackendApi::Controllers::ApplicationsController
    use BackendApi::Controllers::DeploysController

    get '/' do
      status 200
      body Presenters::RootRepresenter.new(nil).to_json
    end
  end
end
