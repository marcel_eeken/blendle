# frozen_string_literal: true

class Error < Sequel::Model
  many_to_one :application
  many_to_one :deploy
end
