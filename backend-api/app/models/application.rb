# frozen_string_literal: true

require 'securerandom'

class Application < Sequel::Model
  many_to_one :user

  one_to_many :deploys
  one_to_many :errors

  def before_create
    super
    self.api_key = generate_api_key
  end

  def generate_api_key
    loop do
      key = SecureRandom.hex(64)
      break key if Application.where(api_key: key).empty?
    end
  end
end
