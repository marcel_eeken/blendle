# frozen_string_literal: true

require 'bcrypt'

class User < Sequel::Model
  attr_accessor :password

  one_to_many :applications

  def deploys(&block)
    Deploy.join(:applications, id: :application_id, &block)
          .join(:users, id: :user_id)
          .select_all(:deploys)
  end

  def before_create
    super
    self.encrypted_password = BCrypt::Password.create(password)
  end

  def self.authenticate(email, password)
    user = find(email: email)

    return nil unless user
    return user if BCrypt::Password.new(user.encrypted_password) == password

    nil
  end
end
