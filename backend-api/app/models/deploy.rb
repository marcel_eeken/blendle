# frozen_string_literal: true

class Deploy < Sequel::Model
  many_to_one :application

  one_to_many :errors

  dataset_module do
    def latest
      order(Sequel.desc(:deploy_date)).first
    end
  end
end
