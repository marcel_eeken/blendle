# frozen_string_literal: true

require 'app/models/application'

module BackendApi
  module Controllers
    class ApplicationsController < Sinatra::Application
      get '/applications/:id' do
        application = find_application_or_halt

        status 200
        body Presenters::ApplicationRepresenter.new(application).to_json
      end

      put '/applications/:id' do
        application = Forms::ApplicationForm.new(find_application_or_halt)

        if application.validate(params[:application])
          application.save

          status 204
        else
          status 400
          body application.errors.messages.to_json
        end
      end

      delete '/applications/:id' do
        application = find_application_or_halt

        application.destroy

        status 204
      end

      post '/applications' do
        authorize!

        application = Forms::ApplicationForm.new(
          Application.new(
            user_id: current_user.id
          )
        )

        if application.validate(params[:application])
          application.save

          status 201
          body Presenters::ApplicationRepresenter.new(application.model).to_json
        else
          status 400
          body application.errors.messages.to_json
        end
      end

      get '/applications' do
        authorize!

        status 200
        body Presenters::ApplicationsRepresenter.new(current_user).to_json
      end

      get '/applications/:id/deploys' do
        application = find_application_or_halt

        status 200
        body Presenters::DeploysRepresenter.new(application).to_json
      end

      get '/applications/:id/deploys/latest' do
        application = find_application_or_halt

        latest_deploy = application.deploys_dataset.latest

        status 200
        body Presenters::DeployRepresenter.new(latest_deploy).to_json
      end

      get '/applications/:id/errors' do
        application = find_application_or_halt

        status 200
        body Presenters::ErrorsRepresenter.new(application).to_json
      end

      private

      def find_application_or_halt
        authorize!

        application = current_user.applications_dataset[id: params[:id]]
        halt 404 if application.nil?

        application
      end
    end
  end
end
