# frozen_string_literal: true

require 'app/models/deploy'

module BackendApi
  module Controllers
    class DeploysController < Sinatra::Application
      post '/deploys' do
        authorize_api!

        deploy = Forms::DeployForm.new(
          Deploy.new(
            deploy_date: DateTime.now,
            application_id: api_application.id
          )
        )

        if deploy.validate(params[:deploy])
          deploy.save

          status 201
          body BackendApi::Presenters::DeployRepresenter.new(deploy.model).to_json
        else
          status 400
          body deploy.errors.messages.to_json
        end
      end

      get '/deploys/:id' do
        deploy = find_deploy_or_halt

        status 200
        body Presenters::DeployRepresenter.new(deploy).to_json
      end

      get '/deploys/:id/errors' do
        deploy = find_deploy_or_halt

        status 200
        body Presenters::ErrorsRepresenter.new(deploy).to_json
      end

      private

      def find_deploy_or_halt
        authorize!

        deploy = current_user.deploys.filter(deploys__id: params[:id]).first
        halt 404 if deploy.nil?

        deploy
      end
    end
  end
end
