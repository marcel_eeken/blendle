# frozen_string_literal: true

require 'app/models/error'

module BackendApi
  module Controllers
    class ErrorsController < Sinatra::Application
      get '/errors/:id' do
        authorize!

        error = find_error_or_halt

        status 200
        body Presenters::ErrorRepresenter.new(error).to_json
      end

      post '/errors' do
        authorize_api!

        error = Forms::ErrorForm.new(
          Error.new(
            application_id: api_application.id,
            deploy_id: api_application.deploys_dataset&.latest&.id
          )
        )

        if error.validate(params[:error])
          error.save

          status 201
          body BackendApi::Presenters::ErrorRepresenter.new(error.model).to_json
        else
          status 400
          body error.errors.messages.to_json
        end
      end

      private

      def find_error_or_halt
        authorize!

        application_ids_for_user = current_user.applications_dataset.select(:id)
        error = Error.where(
          application_id: application_ids_for_user,
          id: params[:id]
        ).limit(1).first

        halt 404 if error.nil?

        error
      end
    end
  end
end
