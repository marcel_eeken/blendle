# frozen_string_literal: true

module BackendApi
  module Controllers
    class SessionsController < Sinatra::Application
      post '/sessions' do
        warden.authenticate
        if warden.authenticated?
          user = warden.user
          status 200
          body BackendApi::Presenters::UserRepresenter.new(user).to_json
        else
          status 401
          body({ message: 'invalid login' }.to_json)
        end
      end
    end
  end
end
