# frozen_string_literal: true

require 'app/models/user'

module BackendApi
  module Controllers
    class UsersController < Sinatra::Application
      get '/users' do
        authorize!

        body BackendApi::Presenters::UserRepresenter.new(current_user).to_json
      end

      put '/users' do
        authorize!

        user_form = Forms::UserEditForm.new(current_user)

        if user_form.validate(params[:user])
          user_form.save

          status 204
        else
          status 400
          body user_form.errors.messages.to_json
        end
      end

      post '/users' do
        user_form = Forms::UserCreateForm.new(User.new)

        if user_form.validate(params[:user])
          user_form.save

          status 201
          body BackendApi::Presenters::UserRepresenter.new(user_form.model).to_json
        else
          status 400
          body user_form.errors.messages.to_json
        end
      end
    end
  end
end
