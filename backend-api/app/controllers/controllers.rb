# frozen_string_literal: true

module BackendApi
  module Controllers
    require 'app/controllers/applications_controller'
    require 'app/controllers/deploys_controller'
    require 'app/controllers/errors_controller'
    require 'app/controllers/sessions_controller'
    require 'app/controllers/users_controller'
  end
end
