# frozen_string_literal: true

module BackendApi
  module Forms
    class UserCreateForm < Reform::Form
      property :name
      property :email
      property :password

      validation :default do
        configure do
          config.messages_file = File.expand_path(
            File.join(
              File.dirname(__FILE__),
              '..',
              '..',
              'config',
              'errors.yml'
            )
          )

          def unique?(value)
            User.find(email: value).nil?
          end
        end

        required(:name).filled(:str?)
        required(:email).filled(:unique?)
        required(:password).filled(:str?)
      end
    end
  end
end
