# frozen_string_literal: true

require 'reform'
require 'reform/form/dry'

Reform::Form.class_eval do
  feature Reform::Form::Dry
end

module BackendApi
  module Forms
    require 'app/forms/application_form'
    require 'app/forms/deploy_form'
    require 'app/forms/error_form'
    require 'app/forms/user_create_form'
    require 'app/forms/user_edit_form'
  end
end
