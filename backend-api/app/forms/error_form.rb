# frozen_string_literal: true

module BackendApi
  module Forms
    class ErrorForm < Reform::Form
      property :message
      property :backtrace

      validation do
        required(:message).filled(:str?)
        required(:backtrace).filled(:array?)
      end
    end
  end
end
