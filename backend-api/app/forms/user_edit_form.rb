# frozen_string_literal: true

module BackendApi
  module Forms
    class UserEditForm < Reform::Form
      property :name

      validation do
        required(:name).filled(:str?)
      end
    end
  end
end
