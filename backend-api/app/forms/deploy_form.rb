# frozen_string_literal: true

module BackendApi
  module Forms
    class DeployForm < Reform::Form
      property :git_sha

      validation do
        required(:git_sha).filled(:str?)
      end
    end
  end
end
