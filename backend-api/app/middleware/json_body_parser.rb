# frozen_string_literal: true
#
# Original from:
# https://github.com/rack/rack-contrib/blob/master/lib/rack/contrib/post_body_content_type_parser.rb
#
# Modified to return the hashes symbolized instead of string keys

require 'json'

module Rack
  class JsonBodyParser
    CONTENT_TYPE = 'CONTENT_TYPE'
    POST_BODY = 'rack.input'
    FORM_INPUT = 'rack.request.form_input'
    FORM_HASH = 'rack.request.form_hash'

    APPLICATION_JSON = 'application/json'

    def initialize(app)
      @app = app
    end

    def call(env)
      return @app.call(env) unless Rack::Request.new(env).media_type == APPLICATION_JSON

      body = env[POST_BODY].read
      env[POST_BODY].rewind # somebody might try to read this stream

      return @app.call(env) if body.empty?

      env.update(
        FORM_HASH => JSON.parse(
          body,
          create_additions: false,
          symbolize_names: true
        ),
        FORM_INPUT => env[POST_BODY]
      )

      @app.call(env)
    end
  end
end
