# frozen_string_literal: true

module Sinatra
  module WardenHelper
    def warden
      env['warden']
    end

    def authenticated?
      return true if warden.authenticated?

      warden.authenticate
      warden.authenticated?
    end

    def user
      warden.user
    end
    alias current_user user

    def authorize!
      return if authenticated?

      halt 401
    end
  end

  helpers WardenHelper
end
