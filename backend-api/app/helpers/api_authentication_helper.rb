# frozen_string_literal: true

module Sinatra
  module ApiAuthenticationHelper
    def authorize_api!
      halt 401 if api_application.nil?
    end

    def api_key
      env['HTTP_AUTHORIZATION']
    end

    def api_application
      @application ||= ::Application.find(api_key: api_key)
    end
  end

  helpers ApiAuthenticationHelper
end
