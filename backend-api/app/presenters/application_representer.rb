# frozen_string_literal: true

module BackendApi
  module Presenters
    class ApplicationRepresenter < Roar::Decorator
      include Roar::JSON::HAL

      property :id
      property :name
      property :api_key

      link :self do
        "/applications/#{represented.id}"
      end

      link :deploys do
        "/applications/#{represented.id}/deploys"
      end

      link :latest_deploy do
        "/applications/#{represented.id}/deploys/latest"
      end

      link :errors do
        "/applications/#{represented.id}/errors"
      end
    end
  end
end
