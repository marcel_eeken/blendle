# frozen_string_literal: true

module BackendApi
  module Presenters
    class ErrorsRepresenter < Roar::Decorator
      include Roar::JSON::HAL

      collection(
        :errors,
        extend: ErrorRepresenter,
        class: Error,
        embedded: true
      )
    end
  end
end
