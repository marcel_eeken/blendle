# frozen_string_literal: true

module BackendApi
  module Presenters
    class ApplicationsRepresenter < Roar::Decorator
      include Roar::JSON::HAL

      collection(
        :applications,
        extend: ApplicationRepresenter,
        class: Application,
        embedded: true
      )
    end
  end
end
