# frozen_string_literal: true

module BackendApi
  module Presenters
    class ErrorRepresenter < Roar::Decorator
      include Roar::JSON::HAL

      property :id
      property :message
      property :backtrace
      property :application_id
      property :deploy_id

      link :self do
        "/errors/#{represented.id}"
      end

      link :application do
        "/applications/#{represented.application_id}"
      end

      link :deploy do
        "/deploys/#{represented.deploy_id}"
      end
    end
  end
end
