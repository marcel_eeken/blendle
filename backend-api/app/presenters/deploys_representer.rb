# frozen_string_literal: true

module BackendApi
  module Presenters
    class DeploysRepresenter < Roar::Decorator
      include Roar::JSON::HAL

      collection(
        :deploys,
        extend: DeployRepresenter,
        class: Deploy,
        embedded: true
      )
    end
  end
end
