# frozen_string_literal: true

module BackendApi
  module Presenters
    class UserRepresenter < Roar::Decorator
      include Roar::JSON::HAL

      property :id
      property :name
      property :email

      link :self do
        "/users/#{represented.id}"
      end

      link :application do
        {
          href: '/applications/{id}',
          templated: true
        }
      end

      link :applications do
        '/applications'
      end
    end
  end
end
