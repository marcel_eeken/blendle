# frozen_string_literal: true

module BackendApi
  module Presenters
    class RootRepresenter < Roar::Decorator
      include Roar::JSON::HAL

      link :sessions do
        '/sessions'
      end

      link :users do
        '/users'
      end

      link :application do
        {
          href: '/applications/{id}',
          templated: true
        }
      end

      link :error do
        {
          href: '/errors/{id}',
          templated: true
        }
      end

      link :deploy do
        {
          href: '/deploys/{id}',
          templated: true
        }
      end
    end
  end
end
