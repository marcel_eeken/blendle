# frozen_string_literal: true

module BackendApi
  module Presenters
    class DeployRepresenter < Roar::Decorator
      include Roar::JSON::HAL

      property :id
      property :git_sha
      property :deploy_date
      property :application_id

      link :self do
        "/deploys/#{represented.id}"
      end

      link :errors do
        "/deploys/#{represented.id}/errors"
      end

      link :application do
        "/applications/#{represented.application_id}"
      end
    end
  end
end
