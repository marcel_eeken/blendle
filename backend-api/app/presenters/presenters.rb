# frozen_string_literal: true

require 'roar/decorator'
require 'roar/json'
require 'roar/json/hal'
require 'roar/json/collection'

module BackendApi
  module Presenters
    require 'app/presenters/application_representer'
    require 'app/presenters/applications_representer'
    require 'app/presenters/deploy_representer'
    require 'app/presenters/deploys_representer'
    require 'app/presenters/error_representer'
    require 'app/presenters/errors_representer'
    require 'app/presenters/user_representer'
    require 'app/presenters/root_representer'
  end
end
