Feature: Managing users
  As an API client
  I want the ability to manage users

  Scenario: Create a user
    Given I send and accept JSON
    When I send a POST request to "/users" with the following
    """
    {
      "user": {
        "name": "Blendle",
        "email": "info@blendle.nl",
        "password": "super_secret"
      }
    }
    """
    Then the response status should be "201"
    And the response should be a "user" JSON representation

  Scenario: Retrieve your user
    Given I send and accept JSON
    When I send a POST request to "/users" with the following
    """
    {
      "user": {
        "name": "Blendle",
        "email": "info@blendle.nl",
        "password": "super_secret"
      }
    }
    """
    When I send a POST request to "/sessions" with the following
    """
    {
      "email": "info@blendle.nl",
      "password": "super_secret"
    }
    """
    When I send a GET request to "/users"
    Then the response status should be "200"
    And the response should be a "user" JSON representation

  Scenario: Update your user
    Given I send and accept JSON
    And I have a logged-in user
    When I send a PUT request to "/users" with the following
    """
    {
      "user": {
        "name": "Telegraaf"
      }
    }
    """
    Then the response status should be "204"
    When I send a GET request to "/users"
    Then the response status should be "200"
    And the response should include
    """
    {
      "name": "Telegraaf"
    }
    """

