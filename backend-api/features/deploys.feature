Feature: Applications
  As an API client
  In order to have more information about my errors
  I want the ability to track my deploys

  Scenario: Create deploy
    Given I send and accept JSON
    And I have a user
    And I have an application with api key "abc" and id "1"
    And I have an application with api key "def" and id "2"
    And I add "def" to the "Authorization" header
    When I send a POST request to "/deploys" with the following
    """
    {
      "deploy": {
        "git_sha": "5a0458b23e2f81ed42fc454d9f3e1945b5f1b7ee"
      }
    }
    """
    Then the response status should be "201"
    And the response should be a "deploy" JSON representation
    And the response should include
    """
    {
      "application_id": 2
    }
    """

  Scenario: Create deploy with wrong api key
    Given I send and accept JSON
    And I have an application with api key "abc" and id "1"
    And I add "def" to the "Authorization" header
    When I send a POST request to "/deploys" with the following
    """
    {
      "deploy": {
        "git_sha": "5a0458b23e2f81ed42fc454d9f3e1945b5f1b7ee"
      }
    }
    """
    Then the response status should be "401"

  Scenario: Get all the errors for a deploy
    Given I send and accept JSON
    And I have a logged-in user
    And I have the following "Application"
      | id | name       | user_id  |
      | 1  | BlendleApp | 1        |
    And I have errors for an application with a deploy and application has id "1"
    When I send a GET request to "/deploys/1/errors"
    Then the response status should be "200"
    And the response should be a "errors" JSON representation
    And the response "_embedded.errors" should have "2" items
