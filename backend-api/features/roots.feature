Feature: Loading the website
  As an API client
  I want to know the routes I can take

  Scenario: Get root
    Given I send and accept JSON
    When I send a GET request to "/"
    Then the response status should be "200"
    And the response should be a "root" JSON representation

