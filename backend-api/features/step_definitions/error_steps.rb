# frozen_string_literal: true

Given(/^I have errors for an application with a deploy and application has id "([^"]*)"$/) do |application_id|
  step 'I have the following "Deploy"', table(%(
      | id | deploy_date                | git_sha | application_id    |
      | 1  | 2016-07-13 16:28:00 +0200  | abcd    | #{application_id} |
  ))

  Error.create(
    message: 'Something went wrong',
    backtrace: [
      'app/models/errors.rb',
      'app/controllers/errors_controller.rb'
    ],
    application_id: application_id,
    deploy_id: 1
  )

  Error.create(
    message: 'Something went wrong',
    backtrace: [
      'app/models/errors.rb',
      'app/controllers/errors_controller.rb'
    ],
    application_id: application_id,
    deploy_id: 1
  )
end
