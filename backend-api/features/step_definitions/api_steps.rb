# frozen_string_literal: true

Given(/^I send and accept JSON$/) do
  header 'Accept', 'application/json'
  header 'Content-Type', 'application/json'
end

Given(/^I add "([^"]*)" to the "([^"]*)" header$/) do |value, field|
  header field, value
end

Given(/^I have the following "([^"]*)"$/) do |model_name, objects|
  model = Object.const_get(model_name)
  model.unrestrict_primary_key
  objects.hashes.each do |object|
    model.create(object)
  end
  model.restrict_primary_key
end

Given(/^I have a user$/) do
  User.unrestrict_primary_key
  User.create(
    id: 1,
    name: 'blendle',
    email: 'info@blendle.nl',
    password: 'super_secret'
  )
  User.restrict_primary_key
end

Given(/^I have a logged\-in user$/) do
  step 'I have the following "User"', table(%(
    | id | name    | email           | password     |
    | 1  | blendle | info@blendle.nl | super_secret |
  ))

  step 'I login as "info@blendle.nl" with "super_secret"'
end

Given(/^I login as "([^\"]*)" with "([^\"]*)"$/) do |email, password|
  step 'I send a POST request to "/sessions" with the following', <<-login_info
  {
    "email": "#{email}",
    "password": "#{password}"
  }
  login_info
end

Given(/^I login as "([^\"]*)" with "([^\"]*)" using BasicAuth$/) do |email, password|
  authorize(email, password)
end

Given(/^I have an application with api key "([^\"]*)" and id "([^\"]*)"$/) do |api_key, id|
  step 'I have the following "Application"', table(%(
    | id     | name       | user_id  |
    | #{id}  | BlendleApp | 1        |
  ))

  Application[id: id].update(api_key: api_key)
end

When(/^I send a GET request to "([^\"]*)"$/) do |path|
  get path
end

When(/^I send a DELETE request to "([^\"]*)"$/) do |path|
  delete path
end

When(/^I send a POST request to "([^\"]*)" with the following$/) do |path, body|
  post(
    path,
    body,
    'CONTENT_TYPE' => 'application/json'
  )
end

When(/^I send a PUT request to "([^\"]*)" with the following$/) do |path, body|
  put(
    path,
    body,
    'CONTENT_TYPE' => 'application/json'
  )
end

Then(/^the response status should be "([^\"]*)"$/) do |status|
  begin
    expect(last_response.status).to eq(status.to_i)
  rescue RSpec::Expectations::ExpectationNotMetError => e
    puts 'Response body:'
    puts last_response.body
    raise e
  end
end

Then(/^the response should be a "([^\"]*)" JSON representation$/) do |model|
  expect(JSON.parse(last_response.body)).to match_response_schema(model)
end

Then(/^the response should match the following$/) do |expected_response|
  expect(JSON.parse(last_response.body)).to include(JSON.parse(expected_response))
end

Then(/^the response "([^\"]*)" should have "([^\"]*)" item|items$/) do |source, size|
  json_response = JSON.parse(last_response.body)
  target = source.split('.')
  expect(json_response.dig(*target).size).to eq(size.to_i)
end

Then(/^the response should include$/) do |data|
  expect(JSON.parse(last_response.body)).to include(JSON.parse(data))
end
