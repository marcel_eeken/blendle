Feature: Sessions
  As an API client
  In order to perform specific tasks
  I need to be logged in

  Scenario: Correct login information
    Given I send and accept JSON
    And I have the following "User"
      | id | name    | email           | password     |
      | 1  | blendle | info@blendle.nl | super_secret |
    When I send a POST request to "/sessions" with the following
    """
    {
      "email": "info@blendle.nl",
      "password": "super_secret"
    }
    """
    Then the response status should be "200"
    And the response should be a "user" JSON representation

  Scenario: Wrong login information
    Given I send and accept JSON
    And I have the following "User"
      | id | name    | email           | password     |
      | 1  | blendle | info@blendle.nl | super_secret |
    When I send a POST request to "/sessions" with the following
    """
    {
      "email": "info@blendle.nl",
      "password": "wrong_password"
    }
    """
    Then the response status should be "401"
    And the response should match the following
    """
    {
      "message": "invalid login"
    }
    """

  Scenario: Use BasicAuth to login
    Given I send and accept JSON
    And I have the following "User"
      | id | name    | email           | password     |
      | 1  | blendle | info@blendle.nl | super_secret |
    And I login as "info@blendle.nl" with "super_secret" using BasicAuth
    When I send a GET request to "/users"
    Then the response status should be "200"
    And the response should be a "user" JSON representation

  Scenario: Use BasicAuth to login with wrong login
    Given I send and accept JSON
    And I have the following "User"
      | id | name    | email           | password     |
      | 1  | blendle | info@blendle.nl | super_secret |
    And I login as "info@blendle.nl" with "wrong_secret" using BasicAuth
    When I send a GET request to "/users"
    Then the response status should be "401"
