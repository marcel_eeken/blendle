Feature: Applications
  As an API client
  In order to log errors
  I need to have applications for which to log them

  Scenario: Create application
    Given I send and accept JSON
    And I have a logged-in user
    When I send a POST request to "/applications" with the following
    """
    {
      "application": {
        "name": "BlendleApp"
      }
    }
    """
    Then the response status should be "201"
    And the response should be a "application" JSON representation

  Scenario: Create application without user
    Given I send and accept JSON
    When I send a POST request to "/applications" with the following
    """
    {
      "application": {
        "name": "BlendleApp"
      }
    }
    """
    Then the response status should be "401"

  Scenario: Get an application
    Given I send and accept JSON
    And I have a logged-in user
    And I have the following "Application"
      | id | name       | user_id  |
      | 1  | BlendleApp | 1        |
    When I send a GET request to "/applications/1"
    Then the response status should be "200"
    And the response should be a "application" JSON representation

  Scenario: Get an application without user
    Given I send and accept JSON
    And I have the following "Application"
      | id | name       | user_id  |
      | 1  | BlendleApp | 1        |
    When I send a GET request to "/applications/1"
    Then the response status should be "401"

  Scenario: Get an application for the wrong user
    Given I send and accept JSON
    And I have the following "User"
      | id | name      | email             | password      |
      | 1  | blendle   | info@blendle.nl   | super_secret  |
      | 2  | telegraaf | info@telegraaf.nl | super_secret2 |
    And I have the following "Application"
      | id | name         | user_id  |
      | 1  | BlendleApp   | 1        |
      | 2  | TelegraafApp | 2        |
    And I login as "info@blendle.nl" with "super_secret"
    When I send a GET request to "/applications/2"
    Then the response status should be "404"

  Scenario: Update an application
    Given I send and accept JSON
    And I have a logged-in user
    And I have the following "Application"
      | id | name       | user_id  |
      | 1  | BlendleApp | 1        |
    When I send a PUT request to "/applications/1" with the following
    """
    {
      "application": {
        "name": "BlendleAwesomeApp"
      }
    }
    """
    Then the response status should be "204"

  Scenario: Update an application without user
    Given I send and accept JSON
    And I have the following "Application"
      | id | name       | user_id  |
      | 1  | BlendleApp | 1        |
    When I send a PUT request to "/applications/1" with the following
    """
    {
      "application": {
        "name": "BlendleAwesomeApp"
      }
    }
    """
    Then the response status should be "401"

  Scenario: Update an application for the wrong user
    Given I send and accept JSON
    And I have the following "User"
      | id | name      | email             | password      |
      | 1  | blendle   | info@blendle.nl   | super_secret  |
      | 2  | telegraaf | info@telegraaf.nl | super_secret2 |
    And I have the following "Application"
      | id | name         | user_id  |
      | 1  | BlendleApp   | 1        |
      | 2  | TelegraafApp | 2        |
    And I login as "info@blendle.nl" with "super_secret"
    When I send a PUT request to "/applications/2" with the following
    """
    {
      "application": {
        "name": "BlendleAwesomeApp"
      }
    }
    """
    Then the response status should be "404"


  Scenario: Delete an application
    Given I send and accept JSON
    And I have a logged-in user
    And I have the following "Application"
      | id | name       | user_id  |
      | 1  | BlendleApp | 1        |
    When I send a DELETE request to "/applications/1"
    Then the response status should be "204"
    When I send a GET request to "/applications/1"
    Then the response status should be "404"

  Scenario: Delete an application without user
    Given I send and accept JSON
    And I have the following "Application"
      | id | name       | user_id  |
      | 1  | BlendleApp | 1        |
    When I send a DELETE request to "/applications/1"
    Then the response status should be "401"

  Scenario: Delete an application for the wrong user
    Given I send and accept JSON
    And I have the following "User"
      | id | name      | email             | password      |
      | 1  | blendle   | info@blendle.nl   | super_secret  |
      | 2  | telegraaf | info@telegraaf.nl | super_secret2 |
    And I have the following "Application"
      | id | name         | user_id  |
      | 1  | BlendleApp   | 1        |
      | 2  | TelegraafApp | 2        |
    And I login as "info@blendle.nl" with "super_secret"
    When I send a DELETE request to "/applications/2"
    Then the response status should be "404"

  Scenario: Overview of applications
    Given I send and accept JSON
    And I have a logged-in user
    And I have the following "Application"
      | id | name       | user_id  |
      | 1  | BlendleApp | 1        |
    When I send a GET request to "/applications"
    Then the response status should be "200"
    And the response should be a "applications" JSON representation
    And the response "_embedded.applications" should have "1" item

  Scenario: Overview of applications without user
    Given I send and accept JSON
    And I have the following "Application"
      | id | name       | user_id  |
      | 1  | BlendleApp | 1        |
    When I send a GET request to "/applications"
    Then the response status should be "401"

  Scenario: Overview of applications with multiple users
    Given I send and accept JSON
    And I have the following "User"
      | id | name      | email             | password      |
      | 1  | blendle   | info@blendle.nl   | super_secret  |
      | 2  | telegraaf | info@telegraaf.nl | super_secret2 |
    And I have the following "Application"
      | id | name              | user_id  |
      | 1  | BlendleApp        | 1        |
      | 3  | BlendleFrontend   | 1        |
      | 2  | TelegraafApp      | 2        |
    And I login as "info@blendle.nl" with "super_secret"
    When I send a GET request to "/applications"
    Then the response status should be "200"
    And the response should be a "applications" JSON representation
    And the response "_embedded.applications" should have "2" items

  Scenario: Get all the deploys for an application
    Given I send and accept JSON
    And I have a logged-in user
    And I have the following "Application"
      | id | name       | user_id  |
      | 1  | BlendleApp | 1        |
    And I have the following "Deploy"
      | id | deploy_date                | git_sha | application_id  |
      | 1  | 2016-07-13 16:28:00 +0200  | abcd    | 1               |
    When I send a GET request to "/applications/1/deploys"
    Then the response status should be "200"
    And the response should be a "deploys" JSON representation

  Scenario: Get the latest deploy for an application
    Given I send and accept JSON
    And I have a logged-in user
    And I have the following "Application"
      | id | name       | user_id  |
      | 1  | BlendleApp | 1        |
    And I have the following "Deploy"
      | id | deploy_date                | git_sha | application_id  |
      | 1  | 2016-07-13 16:28:00  | abcd    | 1               |
      | 2  | 2016-07-15 16:28:00  | efgh    | 1               |
      | 3  | 2016-07-14 16:28:00  | ijkl    | 1               |
    When I send a GET request to "/applications/1/deploys/latest"
    Then the response status should be "200"
    And the response should be a "deploy" JSON representation
    And the response should include
    """
    {
      "deploy_date": "2016-07-15 16:28:00 +0000"
    }
    """

  Scenario: Get all the errors for an application
    Given I send and accept JSON
    And I have a logged-in user
    And I have the following "Application"
      | id | name       | user_id  |
      | 1  | BlendleApp | 1        |
    And I have errors for an application with a deploy and application has id "1"
    When I send a GET request to "/applications/1/errors"
    Then the response status should be "200"
    And the response should be a "errors" JSON representation
    And the response "_embedded.errors" should have "2" items
