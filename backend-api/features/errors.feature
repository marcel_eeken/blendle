Feature: Errors
  As an API client
  In order to monitor my application
  I want the ability to track my errors

  Scenario: Create error
    Given I send and accept JSON
    And I have a user
    And I have an application with api key "abc" and id "1"
    And I have an application with api key "def" and id "2"
    And I add "def" to the "Authorization" header
    When I send a POST request to "/errors" with the following
    """
    {
      "error": {
        "message": "Something went wrong",
        "backtrace": [
          "app/models/errors.rb",
          "app/controllers/errors_controller.rb"
        ]
      }
    }
    """
    Then the response status should be "201"
    And the response should be a "error" JSON representation
    And the response should include
    """
    {
      "application_id": 2
    }
    """

  Scenario: Create deploy
    Given I send and accept JSON
    And I have a user
    And I have an application with api key "abc" and id "1"
    And I have the following "Deploy"
      | id | deploy_date          | git_sha | application_id  |
      | 1  | 2016-07-13 16:28:00  | abcd    | 1               |
      | 2  | 2016-07-15 16:28:00  | efgh    | 1               |
      | 3  | 2016-07-14 16:28:00  | ijkl    | 1               |
    And I add "abc" to the "Authorization" header
    When I send a POST request to "/errors" with the following
    """
    {
      "error": {
        "message": "Something went wrong",
        "backtrace": [
          "app/models/errors.rb",
          "app/controllers/errors_controller.rb"
        ]
      }
    }
    """
    Then the response status should be "201"
    And the response should be a "error" JSON representation
    And the response should include
    """
    {
      "deploy_id": 2
    }
    """

  Scenario: Create error with wrong api key
    Given I send and accept JSON
    And I have an application with api key "abc" and id "1"
    And I add "def" to the "Authorization" header
    When I send a POST request to "/errors" with the following
    """
    {
      "error": {
        "message": "Something went wrong",
        "backtrace": [
          "app/models/errors.rb",
          "app/controllers/errors_controller.rb"
        ]
      }
    }
    """
    Then the response status should be "401"

  Scenario: Get an error
    Given I send and accept JSON
    And I have an application with api key "abc" and id "1"
    And I have the following "Error"
      | id | message    |  application_id  |
      | 1  | Big Error  |  1               |
    And I have a logged-in user
    When I send a GET request to "/errors/1"
    Then the response status should be "200"
    And the response should be a "error" JSON representation

  Scenario: Get an error without a user
    Given I send and accept JSON
    And I have an application with api key "abc" and id "1"
    And I have the following "Error"
      | id | message    |  application_id  |
      | 1  | Big Error  |  1               |
    When I send a GET request to "/errors/1"
    Then the response status should be "401"

  Scenario: Get an error with the wrong user
    Given I send and accept JSON
    And I have the following "User"
      | id | name      | email             | password      |
      | 1  | blendle   | info@blendle.nl   | super_secret  |
      | 2  | telegraaf | info@telegraaf.nl | super_secret2 |
    And I have the following "Application"
      | id | name         | user_id  |
      | 1  | BlendleApp   | 1        |
      | 2  | TelegraafApp | 2        |
    And I have the following "Error"
      | id | message       |  application_id  |
      | 1  | Big Error     |  1               |
      | 2  | Bigger Error  |  2               |
    And I login as "info@blendle.nl" with "super_secret"
    When I send a GET request to "/errors/2"
    Then the response status should be "404"
