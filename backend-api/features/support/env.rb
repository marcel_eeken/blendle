# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'

require 'simplecov'
SimpleCov.start

require File.join(File.dirname(__FILE__), '..', '..', 'backend_api.rb')

require 'rspec'
require 'rack/test'

require 'json-schema'

require 'database_cleaner'
require 'database_cleaner/cucumber'

DatabaseCleaner.strategy = :truncation

RSpec::Matchers.define :match_response_schema do |model|
  match do |response|
    schema_dir = File.join(File.dirname(__FILE__), '..', 'schema_definitions')
    schema_path = File.join(schema_dir, "#{model}_schema.json")
    JSON::Validator.validate!(schema_path, response)
  end
end

class BackendApiWorld
  include RSpec::Expectations
  include RSpec::Matchers
  include Rack::Test::Methods

  def app
    BackendApi::App
  end
end

World do
  BackendApiWorld.new
end

Around do |_scenario, block|
  DatabaseCleaner.cleaning(&block)
end
