# frozen_string_literal: true

require './backend_api'

run BackendApi::App
