#!/bin/bash
set -x

bundle check || bundle install
bundle exec rake db:migrate
bundle exec rake db:seed
exec bundle exec rackup -o 0.0.0.0 -p 4567
